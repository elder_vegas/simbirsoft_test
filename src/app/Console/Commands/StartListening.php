<?php

namespace App\Console\Commands;

use App\Services\MessageService;
use Illuminate\Console\Command;

class StartListening extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start:listening';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start process listening queue.';

    /**
     * Execute the console command.
     *
     * @throws \ErrorException
     * @return void
     */
    public function handle()
    {
        (new MessageService())->listen();
    }
}
