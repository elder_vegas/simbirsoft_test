<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class MessageService
{
    /** @var string */
    private $connectionName = 'rabbitmq';

    /** @var string */
    private $queue = 'default';

    /** @var AMQPStreamConnection */
    private $connection;

    /** @var AMQPChannel */
    private $channel;

    /**
     * @param $connection
     */
    public function useConnection($connection)
    {
        if (array_search($connection, array_keys(config('amqp'))) !== false) {
            $this->connectionName = $connection;
        }
    }

    /**
     * @param $queue
     */
    public function onQueue($queue)
    {
        $this->queue = $queue;
    }

    /**
     * @param string $message
     * @return bool
     */
    public function send($message)
    {
        $this->createConnectionsAndChannel();

        $this->channel->basic_publish(new AMQPMessage($message), '', $this->queue);

        $this->closeAll();

        return true;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return Cache::get('messages') ?? [];
    }

    /**
     * @throws \ErrorException
     * @return void
     */
    public function listen()
    {
        $this->createConnectionsAndChannel();

        $callback = function($msg) {
            $messages = $this->getAll();
            $messages[] = $msg->body;
            Cache::forever('messages', $messages);
        };
        $this->channel->basic_consume($this->queue, '', false, true, false, false, $callback);
        while(count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        $this->closeAll();
    }

    private function createConnectionsAndChannel()
    {
        $this->connection = new AMQPStreamConnection(
            config("amqp.{$this->connectionName}.host"),
            config("amqp.{$this->connectionName}.port"),
            config("amqp.{$this->connectionName}.user"),
            config("amqp.{$this->connectionName}.password")
        );
        $this->channel = $this->connection->channel();
        $this->channel->queue_declare($this->queue, false, false, false, false);
    }

    private function closeAll()
    {
        $this->channel->close();
        $this->connection->close();
    }
}