<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageStoreRequest;
use App\Services\MessageService;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(MessageService $service)
    {
        $messages = array_reverse($service->getAll());

        return view('messages.index', ['messages' => $messages]);
    }

    public function create()
    {
        return view('messages.create');
    }

    public function store(MessageStoreRequest $request, MessageService $service)
    {
        $service->send(auth()->user()->name . ' says: ' . $request->get('message'));

        return redirect()->back()->with('status', 'Message has success added!');
    }
}
