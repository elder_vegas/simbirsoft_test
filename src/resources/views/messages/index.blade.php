@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(empty($messages))
                    <div class="card">
                        <div class="card-body">
                            Nobody has written anything here yet :'(
                        </div>
                    </div>
                @else
                    <div class="card">
                        <ul class="list-group list-group-flush">
                            @foreach($messages as $message)
                                <li class="list-group-item">
                                    {{ $message }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
